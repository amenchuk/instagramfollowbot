package com.instagramfollowbot.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.io.File;
import java.time.Duration;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import static com.instagramfollowbot.Information.errorMessage;
import static com.instagramfollowbot.Information.infoMessage;

public class WebDriverPages {

    private static Wait<WebDriver> wait;
    private final String MODULE_NAME = "WEBDRIVER";
    private final String BUTTON_ACCEPT_COOKIES = "//button[text()='Accept']";
    private final String FIELD_LOGIN = "//input[@name='username']";
    private final String FIELD_PASSWORD = "//input[@name='password']";
    private final String BUTTON_LOGIN = "//button[@type='submit']";
    private final String BUTTON_NOTNOW_NOTIFICATION = "//div[@role='dialog']//button[2]";
    private final String BUTTON_SAVEINFO_NOT_NOW = "//button[text()='Save Info']";
    private final String LABEL_USERNAME = "//header/section/div[1]/*[1]";
    private final String LABEL_ERROR_MESSAGE = "//div/h2[contains(text(),'Sorry,')]";
    private final String LABEL_ERROR_BANER = "//h3[text()='Try Again Later']";
    private final String BUTTON_FOLLOW_FOLLOWBACK = "//section/div//button[text()='Follow' or text()='Follow Back']";
    private final String BUTTON_FOLLOWING_REQUESTED = "//button[text()='Requested']|//span[@aria-label='Following']";
    private final String BUTTON_UNFOLLOW = "//div[@role='dialog']//button[contains(text(),'Unfollow')]";
    private final String FIRST_POST = "//div[@style]/div[1]/div[1]/a/div";
    private final String LIKE_BUTTON = "//section/span[1]/button/div/span//*[@aria-label='Like']";

    private final int longPause = 3600; /* 60 minutes */

    public WebDriver driver;

    public WebDriverPages() {
        setupDriver();
    }

    public int getShortPause() {
        return ThreadLocalRandom.current().nextInt(23, 28);
    }

    private void setupDriver() {
        File file = new File("Driver/chromedriver.exe");
        System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--lang=en-GB");
        options.setCapability("applicationCacheEnabled", true);
        options.addArguments("disable-application-cache");
        options.addArguments("disk-cache-size=0");

        driver = new ChromeDriver(options);
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        wait = new FluentWait<>(driver).
                withMessage("Element was not found").
                withTimeout(Duration.ofSeconds(10)).
                pollingEvery(Duration.ofMillis(100));
    }

    public void goToMainPage(String instagramLoginPage) {
        driver.get(instagramLoginPage);
    }

    public void login(String login, String password) {
        try {
            waitAndClick(driver.findElement(By.xpath(BUTTON_ACCEPT_COOKIES)));
            waitAndType(driver.findElement(By.xpath(FIELD_LOGIN)), login);
            waitAndType(driver.findElement(By.xpath(FIELD_PASSWORD)), password);
            waitAndClick(driver.findElement(By.xpath(BUTTON_LOGIN)));

            try {
                if (driver.findElement(By.xpath(BUTTON_SAVEINFO_NOT_NOW)).isDisplayed()) {
                    waitElement(driver.findElement(By.xpath(BUTTON_SAVEINFO_NOT_NOW)));
                    waitAndClick(driver.findElement(By.xpath(BUTTON_SAVEINFO_NOT_NOW)));
                }
            } catch (NoSuchElementException e) {
                infoMessage(MODULE_NAME, "The 'Save Your Login Info?' window has not been displayed.");
            }

            try {
                if (driver.findElement(By.xpath(BUTTON_NOTNOW_NOTIFICATION)).isDisplayed()) {
                    waitElement(driver.findElement(By.xpath(BUTTON_NOTNOW_NOTIFICATION)));
                    waitAndClick(driver.findElement(By.xpath(BUTTON_NOTNOW_NOTIFICATION)));
                }
            } catch (NoSuchElementException e) {
                infoMessage(MODULE_NAME, "The 'Allow Notification' window has not been displayed.");
            }

            infoMessage(MODULE_NAME, "Login as '" + login.toUpperCase() + "' was successful.");
        } catch (NoSuchElementException e) {
            errorMessage(MODULE_NAME, "Something wrong during login. Check the possibility manually.");
            driverShutDownWithCode(1);
        }
    }

    public boolean checkAndGoToUserPage(String path_toUserPage) {
        driver.navigate().to(path_toUserPage);
        try {
            waitElement(driver.findElement(By.xpath(LABEL_USERNAME)));
            infoMessage(MODULE_NAME, "User '" + driver.findElement(By.xpath(LABEL_USERNAME)).getText().toUpperCase() + "'");
            return true;
        } catch (NoSuchElementException e) {
            if (driver.findElement(By.xpath(LABEL_ERROR_MESSAGE)).isDisplayed()) {
                return false;
            } else {
                errorMessage(MODULE_NAME, "Something was wrong during going to the user page.");
            }
            return false;
        }
    }

    public boolean following() {
        infoMessage(MODULE_NAME, "Following - start");

        if (isErrorLabelDisplayed()) return false;

        try {
            waitAndClick(driver.findElement(By.xpath(BUTTON_FOLLOW_FOLLOWBACK)));

            if (isTryAgainErrorBannerDisplayed()) return false;

            waitFor(getShortPause());
            driver.navigate().refresh();

            try {
                if (driver.findElement(By.xpath(BUTTON_FOLLOWING_REQUESTED)).isDisplayed()) {
                    infoMessage(MODULE_NAME, "Following - success");
                    return true;
                }
            } catch (NoSuchElementException e) {
                if (driver.findElement(By.xpath(BUTTON_FOLLOW_FOLLOWBACK)).isDisplayed()) {
                    printInstagramBlockerMessageAndWait(longPause);
                    return false;
                } else {
                    errorMessage(MODULE_NAME, "Something was wrong during asserting that 'Requested' or 'Following' buttons are displaying.");
                    driverShutDownWithCode(1);
                }
            }
        } catch (NoSuchElementException e) {
            if (driver.findElement(By.xpath(BUTTON_FOLLOWING_REQUESTED)).isDisplayed()) {
                infoMessage(MODULE_NAME, "You are already followed");
                return true;
            } else {
                errorMessage(MODULE_NAME, "Something was wrong during following!");
                driverShutDownWithCode(1);
            }
        }
        return false;
    }

    public boolean unFollowing() {
        infoMessage(MODULE_NAME, "Unfollowing - start");

        if (isErrorLabelDisplayed()) return false;

        try {
            waitAndClick(driver.findElement(By.xpath(BUTTON_FOLLOWING_REQUESTED)));
            waitAndClick(driver.findElement(By.xpath(BUTTON_UNFOLLOW)));

            if (isTryAgainErrorBannerDisplayed()) return false;

            waitFor(getShortPause());
            driver.navigate().refresh();

            try {
                if (driver.findElement(By.xpath(BUTTON_FOLLOW_FOLLOWBACK)).isDisplayed()) {
                    infoMessage(MODULE_NAME, "Unfollowing - success");
                    return true;
                }
            } catch (NoSuchElementException e1) {
                try {
                    driver.navigate().refresh();

                    if (driver.findElement(By.xpath(BUTTON_FOLLOW_FOLLOWBACK)).isDisplayed()) {
                        infoMessage(MODULE_NAME, "Unfollowing - success from 2nd attempt");
                        return true;
                    }
                } catch (NoSuchElementException e2) {
                    if (driver.findElement(By.xpath(BUTTON_FOLLOWING_REQUESTED)).isDisplayed()) {
                        printInstagramBlockerMessageAndWait(longPause);
                        return false;
                    } else {
                        errorMessage(MODULE_NAME, "Something was wrong during asserting that 'Follow' or 'Follow back' buttons are displaying.");
                        driverShutDownWithCode(1);
                    }
                }
            }
        } catch (NoSuchElementException e) {
            if (driver.findElement(By.xpath(BUTTON_FOLLOW_FOLLOWBACK)).isDisplayed()) {
                infoMessage(MODULE_NAME, "You are already unfollowed");
                return true;
            } else {
                errorMessage(MODULE_NAME, "Something was wrong during unfollowing!");
                driverShutDownWithCode(1);
            }
        }
        return false;
    }

    public boolean likeFirstPostIfExist() {
        try {
            infoMessage(MODULE_NAME, "1st post - allocating.");

            waitAndClick(driver.findElement(By.xpath(FIRST_POST)));
            waitAndClick(driver.findElement(By.xpath(LIKE_BUTTON)));
            infoMessage(MODULE_NAME, "1st post - liked.");

            try {
                if (driver.findElement(By.xpath(LABEL_ERROR_BANER)).isDisplayed()) {
                    infoMessage(MODULE_NAME, "1st post - like skipped due to blocking by instagram.");
                    return false;
                }
            }catch (NoSuchElementException e){}

        } catch (NoSuchElementException e) {
            infoMessage(MODULE_NAME, "1st post - like skipped.");
            return true;
        }
        return true;
    }

    private boolean isErrorLabelDisplayed() {
        try {
            if (driver.findElement(By.xpath(LABEL_ERROR_MESSAGE)).getText().contains("Error")) {
                infoMessage(MODULE_NAME, "The 'Error' label is displaying.");
                printInstagramBlockerMessageAndWait(longPause);
                return true;
            }
        } catch (NoSuchElementException eq) {
            return false;
        }
        return false;
    }

    private boolean isTryAgainErrorBannerDisplayed() {
        try {
            if (driver.findElement(By.xpath(LABEL_ERROR_BANER)).getText().contains("Try Again Later")) {
                infoMessage(MODULE_NAME, "The 'Try Again Later' banner is displaying.");
                printInstagramBlockerMessageAndWait(longPause);
                return true;
            }
        } catch (NoSuchElementException eq) {
            return false;
        }
        return false;
    }

    public void driverShutDownWithCode(int statusCode) {
        infoMessage(MODULE_NAME, "Program was closed with status code:" + statusCode);
        driver.quit();
        System.exit(statusCode);
    }

    public void waitFor(int seconds) {
        try {
            TimeUnit.SECONDS.sleep(seconds);
        } catch (InterruptedException e) {
            errorMessage(MODULE_NAME, "Error during sleeping!");
            driverShutDownWithCode(1);
        }
    }

    private void printInstagramBlockerMessageAndWait(int pauseInSeconds) {
        infoMessage(MODULE_NAME, "Perhaps Instagram blocked many requests.");
        infoMessage(MODULE_NAME, "Try to wait for " + getLongPauseInMinutes() + " minutes.");
        waitFor(pauseInSeconds);
    }

    protected void waitElement(WebElement element) {
        wait.until(ExpectedConditions.visibilityOf(element));
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    protected void waitAndType(WebElement element, String text) {
        waitElement(element);
        element.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
        element.clear();
        element.click();
        element.sendKeys(text);
    }

    protected void waitAndClick(WebElement element) {
        waitElement(element);
        element.click();
    }

    public long getLongPauseInMinutes() {
        return longPause / 60;
    }
}
