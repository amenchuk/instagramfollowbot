package com.instagramfollowbot;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Information {

    public static void infoMessage(String moduleName, String message) {
        System.out.println(
                new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").
                        format(new Date()) + "\t[" + moduleName + "]\t" + message);
    }

    public static void printOperationDuration(String moduleName, double seconds) {
        System.out.println(
                new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").
                        format(new Date()) + "\t[" + moduleName + "]\t\tDone in " + (int) seconds + " seconds.");
    }

    public static void errorMessage(String moduleName, String message) {
        System.err.println(
                new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").
                        format(new Date()) + "\t[" + moduleName + "]\t" + message);
    }

    public static String getLineOfSigns(String sign, int quantityInLine) {
        StringBuilder line = new StringBuilder();
        for (int i = 0; i < quantityInLine; i++) {
            line.append(sign);
        }
        return line.toString();
    }


}

