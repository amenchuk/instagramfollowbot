package com.instagramfollowbot;

import com.instagramfollowbot.database.Database;
import com.instagramfollowbot.database.ParserJSON2Database;
import com.instagramfollowbot.pages.WebDriverPages;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;

import java.util.Date;

import static com.instagramfollowbot.Information.*;

public class Application {

    private static final String INSTAGRAM_URL = "https://www.instagram.com/";
    private static final String LOGIN_PAGE_PATH = "accounts/login/?source=auth_switcher";
    private static final String MODULE_NAME = "MAIN";

    private static final WebDriverPages WEB_DRIVER_PAGES = new WebDriverPages();

    private static final int tries = 10;
    private static int tryCounter = 0;
    private static Database database;

    public static void main(String[] args) {

        if (args.length < 4) {
            errorMessage(MODULE_NAME, "Format of line is not correct:\n" +
                    "\t[FORMAT]\tjava -jar InstagramFollowBot.jar {login_or_phone} {password} {-addForFollow|-markSubscribed|-none} {-follow|-unFollow}");
            WEB_DRIVER_PAGES.driverShutDownWithCode(1);
        }

        String login = args[0];
        String password = args[1];
        String actionFiles = args[2];
        String actionDriver = args[3];

        /*Connect to the DataBase or create if not exist.*/
        database = new Database();

        switch (actionFiles) {
            case "-addForFollow":
                /*Parse data from JSONfiles\*.json files if they are exist*/
                new ParserJSON2Database(database).parseToDataBase();
                break;
            case "-markSubscribed":
                new ParserJSON2Database(database).markSubscriberAtDataBase();
                break;
            case "-none":
                infoMessage(MODULE_NAME, "Parsing skipped.\n");
                break;
            default:
                errorMessage(MODULE_NAME, "Third parameter should be one of:\n" +
                        "\t\t\t '-addForFollow' - for adding new users do DB from json files in 'JSONfilesForFollow' folder.\n" +
                        "\t\t\t '-markSubscribed' - for adding new users do DB from json files in 'JSONfilesOfSubscribers' folder.\n" +
                        "\t\t\t '-none' - for skip parsing.\n");
                WEB_DRIVER_PAGES.driverShutDownWithCode(1);
                break;
        }

        try {
            login(login, password);

            switch (actionDriver) {
                case "-follow":
                    follow(login);
                    break;
                case "-unFollow":
                    unfollow(login);
                    break;
                default:
                    errorMessage(MODULE_NAME, "Fourth parameter should be one of:\n" +
                            "\t\t\t '-follow' - for following.\n" +
                            "\t\t\t '-unFollow' - for unfollowing.\n");
                    WEB_DRIVER_PAGES.driverShutDownWithCode(1);
                    break;
            }
        } catch (TimeoutException | ElementClickInterceptedException e) {
            errorMessage(MODULE_NAME, e.getMessage());
            WEB_DRIVER_PAGES.driverShutDownWithCode(1);
        }

        WEB_DRIVER_PAGES.driverShutDownWithCode(0);
    }

    private static void login(String login, String password) {
        WEB_DRIVER_PAGES.goToMainPage(INSTAGRAM_URL + LOGIN_PAGE_PATH);
        WEB_DRIVER_PAGES.login(login, password);
        database.getDatabaseFollowingStatus();
    }

    private static void follow(String pageName) {
        String userName = database.getNotRequestedUser()[1];

        while (userName != null) {
            Date startFollowingDate = new Date();

            System.out.println();
            Information.infoMessage(MODULE_NAME, "\t" + pageName.toUpperCase());

            try {
                if (!WEB_DRIVER_PAGES.checkAndGoToUserPage(INSTAGRAM_URL + userName)) {
                    database.markAsDeleted(userName);
                    userName = database.getNotRequestedUser()[1];
                    database.getDatabaseFollowingStatus();

                    printOperationDuration(MODULE_NAME, getTimeRangeInSeconds(startFollowingDate, new Date()));
                } else {
                    if (WEB_DRIVER_PAGES.following()) {
                        database.markAsRequested(userName);

                        WEB_DRIVER_PAGES.likeFirstPostIfExist();
                        database.getDatabaseFollowingStatus();

                        printOperationDuration(MODULE_NAME, getTimeRangeInSeconds(startFollowingDate, new Date()));

                        userName = database.getNotRequestedUser()[1];
                        tryCounter = 0;
                    } else {
                        checkBlockingByInstagram();
                    }
                }
            } catch (TimeoutException | StaleElementReferenceException e) {
                WEB_DRIVER_PAGES.driver.navigate().refresh();
                WEB_DRIVER_PAGES.waitFor(WEB_DRIVER_PAGES.getShortPause());
            }
        }
    }

    private static void unfollow(String pageName) {
        String userName = database.getRequestedFollowedUser()[1];

        while (userName != null) {
            Date startUnfollowingDate = new Date();

            System.out.println();
            Information.infoMessage(MODULE_NAME, "\t" + pageName.toUpperCase());

            try {
                if (!WEB_DRIVER_PAGES.checkAndGoToUserPage(INSTAGRAM_URL + userName)) {
                    database.markAsDeleted(userName);
                    userName = database.getRequestedFollowedUser()[1];
                    database.getDatabaseFollowingStatus();

                    printOperationDuration(MODULE_NAME, getTimeRangeInSeconds(startUnfollowingDate, new Date()));
                } else {
                    if (WEB_DRIVER_PAGES.unFollowing()) {
                        database.markAsUnfollowed(userName);

                        WEB_DRIVER_PAGES.likeFirstPostIfExist();
                        database.getDatabaseFollowingStatus();

                        printOperationDuration(MODULE_NAME, getTimeRangeInSeconds(startUnfollowingDate, new Date()));

                        userName = database.getRequestedFollowedUser()[1];
                        tryCounter = 0;
                    } else {
                        checkBlockingByInstagram();
                    }
                }
            } catch (TimeoutException | StaleElementReferenceException e) {
                WEB_DRIVER_PAGES.driver.navigate().refresh();
                WEB_DRIVER_PAGES.waitFor(WEB_DRIVER_PAGES.getShortPause());
            }
        }
    }

    private static double getTimeRangeInSeconds(Date start, Date end) {
        return (end.getTime() - start.getTime()) / 1000;
    }

    private static void checkBlockingByInstagram() {
        if (tryCounter >= tries) {
            errorMessage(MODULE_NAME, "Instagram blocked requests more than "
                    + tries * WEB_DRIVER_PAGES.getLongPauseInMinutes() + " minutes. Try to wait few hours and restart the bot.");
            WEB_DRIVER_PAGES.driverShutDownWithCode(0);
        }
        tryCounter++;
    }
}
