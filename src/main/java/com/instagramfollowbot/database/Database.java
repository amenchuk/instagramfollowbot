package com.instagramfollowbot.database;

import com.instagramfollowbot.database.contract.User;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.instagramfollowbot.Information.*;

public class Database {

    private static final String DATABASE_PATH = "DataBase/InstagramFollowBotDatabase.db";
    private static final String SQL_URL = "jdbc:sqlite:" + DATABASE_PATH;

    private static final String USER_ID = "user_id";
    private static final String FULL_NAME = "full_name";
    private static final String USER_NAME = "user_name";
    private static final String IS_PRIVATE = "isPrivate";
    private static final String REQUESTED_BY_VIEWER = "requested_by_viewer";
    private static final String IS_VERIFIED = "is_verified";
    private static final String FOLLOWED_BY_VIEWER = "followed_by_viewer";
    private static final String IS_REQUESTED = "isRequested";
    private static final String REQUEST_DATE = "request_date";
    private static final String MODULE_NAME = "DATABASE";

    private static final String CREATE_USERS_TABLE_QUERY = "CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY AUTOINCREMENT, user_id text, full_name text, user_name text, isPrivate text, requested_by_viewer text, is_verified text, followed_by_viewer text, isRequested text, request_date text, isSubscriber text)";
    private static final String ADD_NEW_USER_QUERY = "INSERT INTO users(user_id, full_name, user_name, isPrivate, requested_by_viewer, is_verified, followed_by_viewer, isRequested, request_date, isSubscriber) VALUES ( ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)";

    private static final String SELECT_NOT_REQUESTED_USER_QUERY = "select * from users where isRequested IS NULL OR isRequested = '' and request_date IS NULL OR request_date = '' ORDER BY id ASC LIMIT 1";
    private static final String SELECT_REQUESTED_FOLLOWED_USER_QUERY = "select * from users where isRequested='true' ORDER BY id ASC LIMIT 1";
    private static final String SELECT_REQUESTED_FOLLOWED_NOT_LIKED_USER_QUERY = "select * from users where isRequested='true' and is_verified='false' ORDER BY id ASC LIMIT 1";
    private static final String SELECT_FOR_DELETING_QUERY = "select * from users where isRequested='true' and isSubscriber='false' ORDER BY id ASC LIMIT 1";

    private static final String MARK_USER_AS_LIKED_QUERY = "update users set is_verified='true' where user_name=?";
    private static final String MARK_USER_AS_REQUESTED_QUERY = "update users set isRequested='true', request_date='" + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()) + "' where user_name=?";
    private static final String MARK_USER_AS_SUBSCRIBER_QUERY = "update users set isSubscriber='true' where user_name=?";
    private static final String MARK_USER_AS_DELETED_QUERY = "update users set isRequested='deleted', request_date='" + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()) + "' where user_name=?";
    private static final String MARK_USER_AS_UNFOLLOWED_QUERY = "update users set isRequested='unfollowed', request_date='" + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()) + "' where user_name=?";

    private static final String SELECT_TOTAL_USERS_COUNT_QUERY = "select count(user_name) as total from users";
    private static final String SELECT_REQUESTED_USERS_COUNT_QUERY = "select count(user_name) as followed from users where isRequested='true'";
    private static final String SELECT_NOT_REQUESTED_USERS_COUNT_QUERY = "SELECT count(user_name) AS notFollowed FROM users WHERE isRequested='false' OR isRequested IS NULL OR isRequested=''";
    private static final String SELECT_REQUESTED_SUBSCRIBERS_COUNT_QUERY = "select count(user_name) as subscriber from users where isSubscriber='true'";
    private static final String SELECT_DELETED_USERS_COUNT_QUERY = "select count(user_name) as deletedU from users where isRequested='deleted'";
    private static final String SELECT_UNFOLLOWED_USERS_COUNT_QUERY = "select count(user_name) as unfollowedU from users where isRequested='unfollowed'";

    private final Connection dbConnection;

    public Database() {
        this.dbConnection = getDatabaseConnection();
        createNewDatabase();
        createUsersTable();
    }

    private Connection getDatabaseConnection() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(SQL_URL);
        } catch (SQLException e) {
            errorMessage(MODULE_NAME, e.getMessage());
        }
        return connection;
    }

    private void createNewDatabase() {
        try {
            if (dbConnection != null) {
                DatabaseMetaData meta = dbConnection.getMetaData();
                infoMessage(MODULE_NAME, "The driver name is " + meta.getDriverName());
                infoMessage(MODULE_NAME, "A new database has been created. \n");
            }
        } catch (SQLException e) {
            errorMessage(MODULE_NAME, e.getMessage());
        }
    }

    private void createUsersTable() {
        Statement stmt = null;
        try {
            stmt = dbConnection.createStatement();
            stmt.execute(CREATE_USERS_TABLE_QUERY);
        } catch (SQLException e) {
            errorMessage(MODULE_NAME, e.getMessage());
        }
    }

    public void addNewUser(User user) {
        try (PreparedStatement preparedStatement = dbConnection.prepareStatement(ADD_NEW_USER_QUERY)) {
            preparedStatement.setString(1, user.getUser_id());
            preparedStatement.setString(2, user.getFull_name());
            preparedStatement.setString(3, user.getUser_name());
            preparedStatement.setString(4, user.getIsPrivate());
            preparedStatement.setString(5, user.getRequested_by_viewer());
            preparedStatement.setString(6, user.getIs_verified());
            preparedStatement.setString(7, user.getFollowed_by_viewer());
            preparedStatement.setString(8, user.getIsRequested());
            preparedStatement.setString(9, user.getRequest_date());
            preparedStatement.setString(10, user.getIsSubscriber());

            preparedStatement.executeUpdate();

            infoMessage(MODULE_NAME, "New user was added:\t" + user.getUser_id() + "\t" + user.getFull_name()
                    + "\t" + user.getUser_name());
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /******************************************************************************************************************/
    public String[] getNotRequestedUser() {
        return getUser(SELECT_NOT_REQUESTED_USER_QUERY);
    }

    public String[] getRequestedUsersAndSubscribersForDeleting() {
        return getUser(SELECT_FOR_DELETING_QUERY);
    }

    public String[] getRequestedFollowedNotLikedUser() {
        return getUser(SELECT_REQUESTED_FOLLOWED_NOT_LIKED_USER_QUERY);
    }

    public String[] getRequestedFollowedUser() {
        return getUser(SELECT_REQUESTED_FOLLOWED_USER_QUERY);
    }

    private String[] getUser(String sqlQuery) {
        String[] result = new String[3];
        try (PreparedStatement preparedStatement = dbConnection.prepareStatement(sqlQuery)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            // loop through the result set
            while (resultSet.next()) {
                result[0] = resultSet.getString(USER_ID);
                result[1] = resultSet.getString(USER_NAME);
                result[2] = resultSet.getString(FULL_NAME);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return result;
    }

    public boolean checkUserExist(String user_name) {
        String SELECT_USER_QUERY = "select * from users where user_name = \"" + user_name + "\"";
        String result = null;
        try (PreparedStatement preparedStatement = dbConnection.prepareStatement(SELECT_USER_QUERY)) {
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                result = rs.getString(USER_ID);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }
        return result != null;
    }

    /******************************************************************************************************************/
    private void markUserInDB(String sqlQuery, String user_name, String asMessage) {
        try {
            PreparedStatement preparedStatement = dbConnection.prepareStatement(sqlQuery);
            preparedStatement.setString(1, user_name);
            preparedStatement.executeUpdate();

            infoMessage(MODULE_NAME, "User '" + user_name.toUpperCase() + "' marked as " + asMessage.toUpperCase() + " in database.");
        } catch (SQLException e) {
            errorMessage(MODULE_NAME, "Error during SQL request mark as '" + asMessage.toUpperCase() + "' " + user_name);
            errorMessage(MODULE_NAME, e.getMessage());
        }
    }

    public void markAsRequested(String user_name) {
        markUserInDB(MARK_USER_AS_REQUESTED_QUERY, user_name, "requested");
    }

   /* public void markAsRequestedSubscriber(String user_name) {
        markUserInDB(MARK_AS_REQUESTED_SUBSCRIBER_QUERY, user_name, "requested subscribers");
    }*/

    public void markAsSubscriber(String user_name) {
        markUserInDB(MARK_USER_AS_SUBSCRIBER_QUERY, user_name, "subscriber");
    }

    public void markAsDeleted(String user_name) {
        markUserInDB(MARK_USER_AS_DELETED_QUERY, user_name, "deleted");
    }

    public void markAsUnfollowed(String user_name) {
        markUserInDB(MARK_USER_AS_UNFOLLOWED_QUERY, user_name, "unfollowed");
    }

    public void markAsLiked(String user_name) {
        markUserInDB(MARK_USER_AS_LIKED_QUERY, user_name, "liked");
    }

    /******************************************************************************************************************/
    private int getCountFor(String sqlQuery, String columnLabel) {
        int result = -1;
        try (PreparedStatement preparedStatement = dbConnection.prepareStatement(sqlQuery)) {
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                result = rs.getInt(columnLabel);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return result;
    }

    private int getTotalUsersCount() {
        return getCountFor(SELECT_TOTAL_USERS_COUNT_QUERY, "total");
    }

    private int getRequestedUsersCount() {
        return getCountFor(SELECT_REQUESTED_USERS_COUNT_QUERY, "followed");
    }

    private int getNotRequestedUsersCount() {
        return getCountFor(SELECT_NOT_REQUESTED_USERS_COUNT_QUERY, "notFollowed");
    }

    private int getRequestedSubscriberCount() {
        return getCountFor(SELECT_REQUESTED_SUBSCRIBERS_COUNT_QUERY, "subscriber");
    }

    private int getDeletedUsersCount() {
        return getCountFor(SELECT_DELETED_USERS_COUNT_QUERY, "deletedU");
    }

    private int getUnfollowedUsersCount() {
        return getCountFor(SELECT_UNFOLLOWED_USERS_COUNT_QUERY, "unfollowedU");
    }

    public void getDatabaseFollowingStatus() {
        infoMessage(MODULE_NAME, getLineOfSigns("-", 83));
        infoMessage(MODULE_NAME, "Total\tNot handled\tHandled\tRequested\tUnfollowed\tDeleted\tSubscribers");
        infoMessage(MODULE_NAME,
                getTotalUsersCount() + "\t" +
                        getNotRequestedUsersCount() + "\t\t" +
                        (getRequestedUsersCount() + getUnfollowedUsersCount() + getDeletedUsersCount()) + "\t" +
                        getRequestedUsersCount() + "\t\t" +
                        getUnfollowedUsersCount() + "\t\t" +
                        getDeletedUsersCount() + "\t" +
                        getRequestedSubscriberCount());
        infoMessage(MODULE_NAME, getLineOfSigns("-", 83));
    }


}