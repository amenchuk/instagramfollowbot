package com.instagramfollowbot.database.contract;

public class User {
    private String user_id;
    private String full_name;
    private String user_name;
    private String isPrivate;
    private String requested_by_viewer;
    private String is_verified;
    private String followed_by_viewer;
    private String isRequested;
    private String request_date;
    private String isSubscriber;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(String isPrivate) {
        this.isPrivate = isPrivate;
    }

    public String getRequested_by_viewer() {
        return requested_by_viewer;
    }

    public void setRequested_by_viewer(String requested_by_viewer) {
        this.requested_by_viewer = requested_by_viewer;
    }

    public String getIs_verified() {
        return is_verified;
    }

    public void setIs_verified(String is_verified) {
        this.is_verified = is_verified;
    }

    public String getFollowed_by_viewer() {
        return followed_by_viewer;
    }

    public void setFollowed_by_viewer(String followed_by_viewer) {
        this.followed_by_viewer = followed_by_viewer;
    }

    public String getIsRequested() {
        return isRequested;
    }

    public void setIsRequested(String isRequested) {
        this.isRequested = isRequested;
    }

    public String getRequest_date() {
        return request_date;
    }

    public void setRequest_date(String request_date) {
        this.request_date = request_date;
    }

    public String getIsSubscriber() {
        return isSubscriber;
    }

    public void setIsSubscriber(String isSubscriber) {
        this.isSubscriber = isSubscriber;
    }
}
