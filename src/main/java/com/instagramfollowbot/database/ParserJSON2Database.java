package com.instagramfollowbot.database;

import com.instagramfollowbot.database.contract.User;
import com.jayway.jsonpath.JsonPath;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import static com.instagramfollowbot.Information.errorMessage;
import static com.instagramfollowbot.Information.infoMessage;

public class ParserJSON2Database {

    private final Database database;
    private final String MODULE_NAME = "PARSER";

    public ParserJSON2Database(Database database) {
        this.database = database;
    }

    public void markSubscriberAtDataBase() {
        File folder = new File("JSONfilesOfSubscribers");
        File[] files = folder.listFiles((d, name) -> name.endsWith(".json"));
        if (files.length <= 0) {
            errorMessage(MODULE_NAME, "'*.json' files not found at '" + folder.getAbsolutePath() + "'\n");
        }
        for (File file : files) {
            if (file.isFile()) {
                infoMessage(MODULE_NAME, "Start marking users from '" + file.getName() + "' file.");
                String filePath = file.getPath();
                List<Object> jsonListOfUsers = JsonPath.read(parseFile(filePath), "$.users.[*]");
                markAtDataBase(jsonListOfUsers);
            }
        }
    }

    private void markAtDataBase(List<Object> jsonListOfUsers) {
        infoMessage(MODULE_NAME, "Mark subscribers at database");
        User user = new User();
        for (Object userJson : jsonListOfUsers) {
            try {
                user.setUser_name(JsonPath.read(userJson, "$.username"));
                if (database.checkUserExist(user.getUser_name())) {
                    database.markAsSubscriber(user.getUser_name());
                } else {
                    errorMessage(MODULE_NAME, "User '" + user.getUser_name() + "' not found at database. Should be added as subscriber.");

                    user.setUser_id("");
                    user.setFull_name("");
                    user.setIsPrivate("false");
                    user.setRequested_by_viewer("false");
                    user.setIs_verified("false");
                    user.setFollowed_by_viewer("false");
                    user.setIsRequested("unfollowed");
                    user.setRequest_date("");
                    user.setIsSubscriber("true");

                    database.addNewUser(user);
                    database.markAsSubscriber(user.getUser_name());
                }
            } catch (Exception e) {
                errorMessage(MODULE_NAME, e.getMessage());
            }
        }
    }

    public void parseToDataBase() {
        File folder = new File("JSONfilesForFollow");
        File[] files = folder.listFiles((d, name) -> name.endsWith(".json"));
        if (files.length <= 0) {
            errorMessage(MODULE_NAME, "'*.json' files not found at '" + folder.getAbsolutePath() + "' \n");
        }
        for (File file : files) {
            if (file.isFile()) {
                infoMessage(MODULE_NAME, "Start parsing '" + file.getName() + "' file.");
                String filePath = file.getPath();
                List<Object> jsonListOfUsers = JsonPath.read(parseFile(filePath), "$.users.[*]");
                pasteToDatabase(jsonListOfUsers);
            }
        }
    }

    private void pasteToDatabase(List<Object> jsonListOfUsers) {
        infoMessage(MODULE_NAME, "Paste users to database.");

        for (Object userJson : jsonListOfUsers) {
            try {
                User user = new User();
                user.setUser_id(JsonPath.read(userJson, "$.id"));
                user.setFull_name(JsonPath.read(userJson, "$.full_name"));
                user.setUser_name(JsonPath.read(userJson, "$.username"));
                user.setIsPrivate(JsonPath.read(userJson, "$.isPrivate"));
                user.setRequested_by_viewer(JsonPath.read(userJson, "$.requested_by_viewer"));
                user.setIs_verified(JsonPath.read(userJson, "$.is_verified"));
                user.setFollowed_by_viewer(JsonPath.read(userJson, "$.followed_by_viewer"));
                if (database.checkUserExist(user.getUser_name())) {
                    errorMessage(MODULE_NAME, "User '" + user.getUser_name() + "' already exists in database.");
                } else {
                    database.addNewUser(user);
                }

            } catch (Exception e) {
                errorMessage(MODULE_NAME, e.getMessage());
            }
        }
    }

    private Object parseFile(String pathToFile) {
        JSONParser parser = new JSONParser();
        Object file = null;
        try {
            try {
                file = parser.parse(new FileReader(pathToFile));
            } catch (IOException e) {
                infoMessage(MODULE_NAME, "File '" + pathToFile + "' not found.\n" + e.getMessage());
                return null;
            }
        } catch (ParseException e) {
            errorMessage(MODULE_NAME, "Something was wrong during the file parsing.\n" + e.getMessage() + "\n");
            return null;
        }
        return file;
    }
}
