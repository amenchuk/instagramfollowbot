## InstagramFollowBot

    The execution file (*.jar) placed at the 'Downloads' section.

##### Description:
The command line script with a web browser component. The script allows automating subscribing at Instagram users.

##### For start the script input to the command line:
```
java -jar InstagramFollowBot.jar {login_or_phone} {password} {-addForFollow|-markSubscribed|-none} {-follow|-unFollow}
```
Parsing parameters:
```
-addForFollow   - for adding new users do DB from json files in 'JSONfilesForFollow' folder.
-markSubscribed - for adding new users do DB from json files in 'JSONfilesOfSubscribers' folder.
-none           - for skip parsing.
```
Action parameters:
```
-follow      - for following.
-unFollow    - for unfollowing.
```

##### Workflow:
1.  Parsing JSON file from 'JSONfiles' folder and copy to the automatically created database file.
2.  Opening a web browser, search and subscribe at the Instagram user (marking at database as isRequested="true")

##### Structure of work folders:

*  InstagramFollowBot_jar
    *  DataBase
        *  {database will create automatically}
    *  Driver
        *  chromedriver.exe
    *  JSONfilesForFollow
        *  {file_name}.json (should be paste manually)
    *  JSONfilesOfSubscribers
        *  {file_name}.json (should be paste manually)
    *  InstagramFollowBot.jar
		
###### Copy the work folders from the 'out' folder.


##### Example of JSON file:
###### For automatically harvesting users json file use [instaFollowersParser_fromHAR](https://bitbucket.org/amenchuk/instafollowersparser_fromhar/overview)

```
{
  "parseDate":"17:00:00 20190320",
  "userCount":2,
  "users": [
    {
      "full_name":"Angela Markoni",
      "id":"6192822421",
      "isPrivate":"true",
      "requested_by_viewer": "false",
      "is_verified": "false",
      "followed_by_viewer":"false",
      "username":"angela_markoni"
    },
    {
      "full_name":"Lena Garastelli",
      "id":"5550724471",
      "isPrivate":"false",
      "requested_by_viewer":"false",
      "is_verified":"false",
      "followed_by_viewer":"false",
      "username":"garasteli.lena"
    }
  ]
}
```